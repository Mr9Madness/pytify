from pytify.core import search_track
from pytify.core import read_config
from pytify.auth import authenticate
import json
import os

config = read_config()
auth = authenticate(config)
results = search_track('The number of the beast', auth)

file = open("data.json", "w")

file.write( json.dumps( results ) )

file.close()